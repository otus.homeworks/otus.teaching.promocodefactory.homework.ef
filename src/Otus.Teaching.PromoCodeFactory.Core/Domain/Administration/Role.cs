﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    [Table("role")]
    [Description("Роли")]
    [Index("Name", IsUnique = true, Name = "RoleName")]

    public class Role
        : BaseEntity
    {
        [MaxLength(100), Required(AllowEmptyStrings = false)]
        public string Name { get; set; }

        [MaxLength(300)]
        public string Description { get; set; }
        public virtual ICollection<Employee> Employees { get; set; }

    }
}