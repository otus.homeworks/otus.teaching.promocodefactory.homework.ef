﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.Profiles
{
    class MapProfile : Profile
    {
        public MapProfile()
        {
            CreateMap<Employee, EmployeeResponse>().IncludeAllDerived();
            CreateMap<PromoCodeShortResponse, PromoCodeShortResponse>().IncludeAllDerived();
        }
    }
}
