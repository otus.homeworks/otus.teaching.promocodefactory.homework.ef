﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : BaseAPIController
    {
        private readonly IUnitOfWork _unitOfWork;
        public CustomersController(IUnitOfWork uow, ILogger<RolesController> logger, AppContextDb context,IMapper mapper) : base(logger, context, mapper)
        {
            _unitOfWork = uow;
        }
        /// <summary>
        /// Получить список клиентов
        /// </summary>
        /// <returns>Список клиентов</returns>
        /// <response code="200">Успешное выполнение</response>
        /// <response code="500">Ошибка API</response>
        [HttpGet]
        [ProducesResponseType(typeof(CustomerShortResponse), 200)]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await _unitOfWork.GetRepository<Customer>().GetAllAsync();
            var response = customers.Select(x => _mapper.Map<CustomerShortResponse>(x)).ToList();

            return Ok(response);
        }

        /// <summary>
        /// получение одного клиента
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(CustomerResponse), 200)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            try
            {
                var customer = await _unitOfWork.GetRepository<Customer>().GetByIdAsync(id, c => c.PromoCodes);
                if (customer == null) return NotFound(new { message = $"Клиент с id={id} не найден" });

                List<PromoCodeShortResponse> listPromo = customer.PromoCodes.Select(x => _mapper.Map<PromoCodeShortResponse>(x)).ToList();
                var preferences = await _unitOfWork.GetRepository<CustomerPreference>().GetByFilterAsync(c => c.CustomerId == id, p => p.Preference);
                List<PreferenceModel> listPreference = preferences.Select(x =>
                new PreferenceModel()
                {
                    Id = x.Id,
                    Name = x.Preference.Name
                }).ToList();
                var response = new CustomerResponse
                {
                    Id = customer.Id,
                    Email = customer.Email,
                    FirstName = customer.FirstName,
                    LastName = customer.LastName,
                    PromoCodes = listPromo,
                    Preferences = listPreference
                };
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Клиент не определен");
                return BadRequest(new { result = "error", errorMessage = "Клиент не определен" + ex.Message });
            }

        }

        /// <summary>
        /// Добавить клиента с его предпочтениями
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            //TODO: Добавить создание нового клиента вместе с его предпочтениями
            try
            {
                var preferenceIds = request.PreferenceIds?.ToArray() ?? Array.Empty<Guid>();
                using var transaction = await _unitOfWork.BeginTransactionAsync();
                try
                {
                    var customerId = Guid.NewGuid();
                    await _unitOfWork.GetRepository<Customer>().AddAsync(new Customer
                    {
                        Id = customerId,
                        FirstName = request.FirstName,
                        LastName = request.LastName,
                        Email = request.Email
                    });
                    if (preferenceIds.Any())
                    {
                        await _unitOfWork.GetRepository<CustomerPreference>().AddRangeAsync(preferenceIds
                            .Select(preferenceId => new CustomerPreference
                            {
                                Id = Guid.NewGuid(),
                                CustomerId = customerId,
                                PreferenceId = preferenceId
                            }));
                    }

                    await transaction.CommitAsync();
                    return Ok(new { result = "ok", Message = "Добавление успешно!" });
                }
                catch
                {
                    await transaction.RollbackAsync();
                    throw;
                }
            }
            catch (Exception ex)
            {

                _logger.LogError(ex, "Ошибка добавления клиента");
                return BadRequest(new { result = ex.HResult, errorMessage = "Ошибка добавления клиента.  " + ex.InnerException.Message ?? ex.Message });
            }

        }
        /// <summary>
        /// Обновить данные клиента вместе с его предпочтениями
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            //TODO: Обновить данные клиента вместе с его предпочтениями
            var customer = await _unitOfWork.GetRepository<Customer>().GetByIdAsync(id);
            if (customer == null) return NotFound(new { message = $"Клиент с id={id} не найден" });
            try
            {
                var preferenceIds = request.PreferenceIds?.ToArray() ?? Array.Empty<Guid>();
                using var transaction = await _unitOfWork.BeginTransactionAsync();
                try
                {
                    customer.Email = request.Email;
                    customer.FirstName = request.FirstName;
                    customer.LastName = request.LastName;

                    await _unitOfWork.GetRepository<Customer>().UpdateAsync(customer);
                    await _unitOfWork.GetRepository<CustomerPreference>().RemoveAsync(c => c.CustomerId == id);

                    if (preferenceIds.Any())
                    {
                        await _unitOfWork.GetRepository<CustomerPreference>().AddRangeAsync(preferenceIds
                            .Select(preferenceId => new CustomerPreference
                            {
                                Id = Guid.NewGuid(),
                                CustomerId = id,
                                PreferenceId = preferenceId
                            }));
                    }

                    await transaction.CommitAsync();
                    return Ok(new { result = "ok", Message = "Корректировка успешна!" });
                }
                catch
                {
                    await transaction.RollbackAsync();
                    throw;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Ошибка корректировки клиента");
                return BadRequest(new { result = "error", errorMessage = "Ошибка корректировки клиента" + ex.InnerException.Message ?? ex.Message });
            }

        }

        /// <summary>
        /// Удалить клиента вместе с выданными ему промо-кодами
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns></returns>
        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            //TODO: Удаление клиента вместе с выданными ему промокодами
            if (!await _unitOfWork.GetRepository<Customer>().AnyAsync(id))
            {
                return NotFound(new { message = $"Клиент с id={id} не найден" });
            }
            try
            {
                using var transaction = await _unitOfWork.BeginTransactionAsync();
                try
                {
                    await _unitOfWork.GetRepository<CustomerPreference>().RemoveAsync(c => c.CustomerId == id);
                    await _unitOfWork.GetRepository<PromoCode>().RemoveAsync(c => c.CustomerId == id);
                    await _unitOfWork.GetRepository<Customer>().RemoveAsync(id);

                    await transaction.CommitAsync();
                    return Ok(new { result = "ok", Message = "Удаление успешно!" });
                }
                catch
                {
                    await transaction.RollbackAsync();
                    throw;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Ошибка удаления клиента");
                return BadRequest(new { result = "error", errorMessage = "Ошибка удаления клиента" + ex.InnerException.Message ?? ex.Message });
            }

        }
    }
}