﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    public class BaseAPIController : ControllerBase
    {
        protected AppContextDb _db;
        protected ILogger _logger;
        protected IMapper _mapper;
        public BaseAPIController(ILogger logger, AppContextDb context,IMapper mapper) : base()
        {
            _logger = logger;
            _db = context;
            _mapper = mapper;
        }
    }
}
